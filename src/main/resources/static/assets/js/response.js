	$(document)
			.ready(
					function() {
						$("#accordion .panel-collapse:last").collapse('toggle');

						$(document.body)
								.append(
										'<footer class="footer"> <div class="container"> <div class="footer-div"> <ul class="footer-links"> <li> <a href="https://github.com/paypal/PayPal-Java-SDK" target="_blank"><i class="fa fa-github"></i> Github</a></li><li> <a href="https://developer.paypal.com/webapps/developer/docs/api/" target="_blank"><i class="fa fa-book"></i> REST API Reference</a> </li><li> <a href="https://github.com/paypal/PayPal-Java-SDK/issues" target="_blank"><i class="fa fa-exclamation-triangle"></i> Report Issues </a> </li></ul> </div></div></footer>');

						$(".prettyprint").each(function() {
							if ($(this).html().trim() != '') {
								try {
									$(this).html(syntaxHighlight(JSON.stringify(JSON.parse($(this).html().trim()), null,2)));
							    } catch (e) {
							        return false;
							    }
							} else {
								$(this).html('No Data');
							}
						});

					});

	/* http://stackoverflow.com/questions/4810841/how-can-i-pretty-print-json-using-javascript */
	function syntaxHighlight(json) {
		json = json.replace(/&/g, '&').replace(/</g, '&lt;').replace(/>/g,
				'&gt;');
		return json
				.replace(
						/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g,
						function(match) {
							var cls = 'number';
							if (/^"/.test(match)) {
								if (/:$/.test(match)) {
									cls = 'key';
									if (match == '"id":') {
										console.log("Matched ID" + match);
										cls = 'key id';
									}
								} else {
									cls = 'string';
								}
							} else if (/true|false/.test(match)) {
								cls = 'boolean';
							} else if (/null/.test(match)) {
								cls = 'null';
							}
							return '<span class="' + cls + '">' + match
									+ '</span>';
						});
	}