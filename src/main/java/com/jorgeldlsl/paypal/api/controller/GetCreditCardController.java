// #GetCreditCard Sample
// This sample code demonstrates how you 
// retrieve a previously saved 
// Credit Card using the 'vault' API.
// API used: GET /v1/vault/credit-card/{id}
package com.jorgeldlsl.paypal.api.controller;

import static com.jorgeldlsl.paypal.api.util.SampleConstants.clientID;
import static com.jorgeldlsl.paypal.api.util.SampleConstants.clientSecret;
import static com.jorgeldlsl.paypal.api.util.SampleConstants.mode;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.jorgeldlsl.paypal.api.util.ResultPrinter;
import com.paypal.api.payments.CreditCard;
import com.paypal.base.rest.APIContext;
import com.paypal.base.rest.PayPalRESTException;

@Controller
public class GetCreditCardController {

	@SuppressWarnings("unused")
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = Logger
			.getLogger(GetCreditCardController.class);

	// ##GetCreditCardUsingId
	// Call the method with a previously created Credit Card ID
	@RequestMapping(value="getcreditcard", method=RequestMethod.GET)
	public String getCreditCard(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		try {
			// ### Api Context
			// Pass in a `ApiContext` object to authenticate
			// the call and to send a unique request id
			// (that ensures idempotency). The SDK generates
			// a request id if you do not pass one explicitly.
			APIContext apiContext = new APIContext(clientID, clientSecret, mode);

			// Retrieve the CreditCard object by calling the
			// static `get` method on the CreditCard class,
			// and pass the APIContext and CreditCard ID
			CreditCard creditCard = CreditCard.get(apiContext,
					"CARD-1P444667NB7245841LEOOMLA");
			LOGGER.info("Credit Card retrieved ID = " + creditCard.getId()
					+ ", status = " + creditCard.getState());
			ResultPrinter.addResult(req, resp, "Got Credit Card from Vault", CreditCard.getLastRequest(), CreditCard.getLastResponse(), null);
		} catch (PayPalRESTException e) {
			ResultPrinter.addResult(req, resp, "Got Credit Card from Vault", CreditCard.getLastRequest(), null, e.getMessage());
		}
		return "response";
	}

}
