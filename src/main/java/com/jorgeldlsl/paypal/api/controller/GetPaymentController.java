// #GetPayment Sample
// This sample code demonstrates how you can retrieve
// the details of a payment resource.
// API used: /v1/payments/payment/{payment-id}
package com.jorgeldlsl.paypal.api.controller;

import static com.jorgeldlsl.paypal.api.util.SampleConstants.clientID;
import static com.jorgeldlsl.paypal.api.util.SampleConstants.clientSecret;
import static com.jorgeldlsl.paypal.api.util.SampleConstants.mode;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.jorgeldlsl.paypal.api.util.ResultPrinter;
import com.paypal.api.payments.Payment;
import com.paypal.base.rest.APIContext;
import com.paypal.base.rest.PayPalRESTException;

@SuppressWarnings("unused")
@Controller
public class GetPaymentController {

	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = Logger
			.getLogger(GetPaymentController.class);

	// ##GetPayment
	// Call the method with a valid Payment ID
	@RequestMapping(value="getpayment", method=RequestMethod.GET)
	public String getPayment(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		try {
			// ### Api Context
			// Pass in a `ApiContext` object to authenticate
			// the call and to send a unique request id
			// (that ensures idempotency). The SDK generates
			// a request id if you do not pass one explicitly.
			APIContext apiContext = new APIContext(clientID, clientSecret, mode);

			// Retrieve the payment object by calling the
			// static `get` method
			// on the Payment class by passing a valid
			// AccessToken and Payment ID
			Payment payment = Payment.get(apiContext,
					"PAY-8US4653704483440NLEOE6XQ");
			LOGGER.info("Payment retrieved ID = " + payment.getId()
					+ ", status = " + payment.getState());
			ResultPrinter.addResult(req, resp, "Get Payment", Payment.getLastRequest(), Payment.getLastResponse(), null);
		} catch (PayPalRESTException e) {
			ResultPrinter.addResult(req, resp, "Get Payment", Payment.getLastRequest(), null, e.getMessage());
		}
		return "response";
	}

}
