package com.jorgeldlsl.paypal.api.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.boot.autoconfigure.web.ErrorViewResolver;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ErrorController implements ErrorViewResolver {
	

	@Override
	public ModelAndView resolveErrorView(HttpServletRequest request, HttpStatus status, Map<String, Object> map) {
		ModelAndView view = new ModelAndView();
		view.setStatus(status);
		view.addAllObjects(map);
		view.setViewName("response");
		return view;
	}

}
