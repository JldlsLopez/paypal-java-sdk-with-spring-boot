// #Get Payout Item Status
// Use this call to get data about a payout item, including the status, without retrieving an entire batch. 
// You can get the status of an individual payout item in a batch in order to review the current status of a previously-unclaimed, or pending, payout item.
// API used: GET /v1/payments/payouts-item/<Payout-Item-Id>

package com.jorgeldlsl.paypal.api.controller;

import static com.jorgeldlsl.paypal.api.util.SampleConstants.clientID;
import static com.jorgeldlsl.paypal.api.util.SampleConstants.clientSecret;
import static com.jorgeldlsl.paypal.api.util.SampleConstants.mode;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.jorgeldlsl.paypal.api.util.ResultPrinter;
import com.paypal.api.payments.PayoutBatch;
import com.paypal.api.payments.PayoutItem;
import com.paypal.api.payments.PayoutItemDetails;
import com.paypal.base.rest.APIContext;
import com.paypal.base.rest.PayPalRESTException;

@Controller
public class GetPayoutItemStatusController {

	@SuppressWarnings("unused")
	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = Logger
			.getLogger(GetPayoutItemStatusController.class);


	// ##Get Payout Item Status
	// Sample showing how to get a Payout Item Status
	@RequestMapping(value="payouts.GetPayoutItemStatus", method=RequestMethod.GET)
	private String payouts_GetPayoutItemStatus(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		getPayoutItemStatus(req, resp);
		return "response";
	}

	public PayoutItemDetails getPayoutItemStatus(HttpServletRequest req,
			HttpServletResponse resp) {

		// ### Get a Payout Batch
		// We are re-using the GetPayoutBatchStatusController to get a batch payout
		// for us. This will make sure the samples will work all the time.
		GetPayoutBatchStatusController servlet = new GetPayoutBatchStatusController();
		PayoutBatch batch = servlet.getPayoutBatchStatus(req, resp);

		// ### Retrieve PayoutItem ID
		// In the samples, we are extractingt he payoutItemId of a payout we
		// just created.
		// In reality, you might be using the payoutItemId stored in your
		// database, or passed manually.
		PayoutItemDetails itemDetails = batch.getItems().get(0);
		String payoutItemId = itemDetails.getPayoutItemId();

		// Initiate the response object
		PayoutItemDetails response = null;
		try {

			// ### Api Context
			// Pass in a `ApiContext` object to authenticate
			// the call and to send a unique request id
			// (that ensures idempotency). The SDK generates
			// a request id if you do not pass one explicitly.
			APIContext apiContext = new APIContext(clientID, clientSecret, mode);

			// ###Get Payout Item
			response = PayoutItem.get(apiContext, payoutItemId);

			LOGGER.info("Payout Item With ID: " + response.getPayoutItemId());
			ResultPrinter.addResult(req, resp, "Got Payout Item Status",
					PayoutItem.getLastRequest(), PayoutItem.getLastResponse(),
					null);
		} catch (PayPalRESTException e) {
			ResultPrinter.addResult(req, resp, "Got Payout Item Status",
					PayoutItem.getLastRequest(), null, e.getMessage());
		}

		return response;
	}
}
