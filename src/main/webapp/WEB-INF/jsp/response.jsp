<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.List"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>PayPal REST API Samples</title>
<link rel="icon" href="/images/favicon.ico">

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<link
	href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css"
	rel="stylesheet">
	
 <link href="/assets/css/response.css" rel="stylesheet">

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script
	src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.2.0/js/scrollspy.min.js"></script>
	
	<script src="/assets/js/response.js"></script>
	
</head>
<body>
	<%
		List<String> responses = (List<String>) request.getAttribute("responses");
		List<String> requests = (List<String>) request.getAttribute("requests");
		List<String> messages = (List<String>) request.getAttribute("messages");
		List<String> errors = (List<String>) request.getAttribute("errors");
	%>
	
	<div class="row header">
		<div class="col-md-5 pull-left">
			<br /> <a href="index.html"><h1 class="home">&#10094;&#10094;
					Back to Samples</h1></a><br />
		</div>
		<br />
		<div class="col-md-4 pull-right">
			<img
				src="https://www.paypalobjects.com/webstatic/developer/logo2_paypal_developer_2x.png"
				class="logo" width="300" />
		</div>
	</div>
	
	<div class="panel-group" id="accordion" role="tablist"
		aria-multiselectable="true">
	<%
		for (int i = 0; i < responses.size(); i++) {
			String resp = responses.get(i);
			String req = requests.get(i);
			String message = messages.get(i);
			String error = errors.get(i);
			String errorClass = (error != null) ? "error" : null;
	%>
	
		<div class="panel panel-default">
			<div class="panel-heading <%= errorClass %>" role="tab" id="heading-<%= i + 1 %>">
				<h4 class="panel-title">
					<a data-toggle="collapse" data-parent="#accordion"
						href="#step-<%= i + 1 %>" aria-expanded="false"
						aria-controls="step-<%= i + 1 %>"><%= i + 1 %>. <%= message %> <% if (error != null) {  %>(Failed)<% } %></a>
				</h4>
			</div>
			<div id="step-<%= i + 1 %>" class="panel-collapse collapse"
				role="tabpanel" aria-labelledby="heading-<%= i + 1 %>">
				<div class="panel-body">
					<%
		if (request.getAttribute("redirectURL") != null) {
	%>
					<div>
						<a href=<%=(String) request.getAttribute("redirectURL")%>>Redirect
							to PayPal to approve the payment</a>
					</div>
					<%
		}
	%>
					<div class="row hidden-xs hidden-sm hidden-md">
						<div class="col-md-6">
							<h4>Request Object</h4>
							<pre class="prettyprint "><%= req %></pre>
						</div>
						<div class="col-md-6">
							<h4 class="<%= errorClass %>">Response Object</h4>
							<% if (error != null) {  %>
								<p class="error"><i class="fa fa-exclamation-triangle"></i> <%= error %></p>
							<% } %>
							<pre class="prettyprint "><%= resp %></pre>
						</div>
					</div>
					<div class="hidden-lg">
						<ul class="nav nav-tabs" role="tablist">
							<li role="presentation"><a href="#step-<%= i + 1 %>-request"
								role="tab" data-toggle="tab">Request</a></li>
							<li role="presentation" class="active"><a
								href="#step-<%= i + 1 %>-response" role="tab" data-toggle="tab">Response</a></li>
						</ul>
						<div class="tab-content">
							<div role="tabpanel" class="tab-pane"
								id="step-<%= i + 1 %>-request">
								<h4>Request Object</h4>
								<pre class="prettyprint "><%= req %></pre>
							</div>
							<div role="tabpanel" class="tab-pane active"
								id="step-<%= i + 1 %>-response">
								<h4>Response Object</h4>
								<pre class="prettyprint "><%= resp %></pre>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<% } %>
	</div>
</body>
</html>