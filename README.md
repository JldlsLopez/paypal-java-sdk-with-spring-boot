# Run the project? #

1. Run project as Spring Boot App and choose PayPalApplication - com.jorgeldlsl.paypal.

2. Go to your browser and write: http://localhost:8080/index

### What is this repository for? ###

* PayPal-Java-SDK-with-Spring-Boot is a project edited from the original code at https://github.com/paypal/PayPal-Java-SDK using Maven instead Gradle, Spring Boot v1.5.4.BUILD-SNAPSHOT and Spring MVC architecture.

* Version 1.0

### Who do I talk to? ###

* https://JldlsLopez@bitbucket.org/JldlsLopez/
* jorgeluisdelossantoslopez@gmail.com